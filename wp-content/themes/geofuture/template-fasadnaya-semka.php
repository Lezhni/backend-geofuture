<?php /* Template Name: Фасадная съемка */ ?>

<?php get_header(); ?>

<div class="modal mfp-hide" id="callback-fasad">
    <span class="modal-close">Закрыть</span>
    <?= do_shortcode( '[contact-form-7 id="254"]' ); ?>
</div>

<section class="block intro intro-exterior-survey">
    <div class="container">
        <h1 class="intro-title"><?= get_field( 'page-title'); ?></h1>
        <h2 class="intro-subtitle"><?= get_field( 'page-subtitle'); ?></h2>
        <p class="intro-description-left">
            <?= get_field( 'fasad-intro-text' ); ?>
        </p>
        <div class="intro-callback-form">
            <?= do_shortcode( '[contact-form-7 id="26"]' ); ?>
        </div>
    </div>
</section>
<?php $fasadTypes = get_field( 'fasad-types' ); ?>

<?php if ( count( $fasadTypes ) > 0 ) : ?>
<section class="block exterior-survey-types">
    <div class="container">
        <h3 class="block-title">Какую фасадную съемку <br>мы делаем</h3>
        <section class="exterior-survey-types-list services-list">

            <?php foreach ( $fasadTypes as $fasadType ) : ?>
            <div class="service-single">
                <h4 class="service-single-title"><?= $fasadType['title']; ?></h4>
                <article class="service-single-text">
                    <p><?= $fasadType['description']; ?></p>
                </article>
                <a href="#callback-fasad" class="button service-single-order popup">Заказать</a>
            </div>
            <?php endforeach; ?>

        </section>
    </div>
</section>
<?php endif; ?>

<?php $portfolio = get_field( 'portfolio' ); ?>
<?php if ( count( $portfolio ) > 0 ) : ?>
<section class="block pattern-block exterior-portfolio portfolio">
    <div class="container">
        <h3 class="block-title">Посмотрите примеры работ, <br>которые мы делаем</h3>
        <div class="portfolio-slider">

            <?php foreach ( $portfolio as $p_item ) : ?>
            <div class="portfolio-slide">

                <?php if ( count( $p_item['photos'] ) > 0 ) : ?>
                <div class="portfolio-slide-gallery">

                    <?php foreach ( $p_item['photos'] as $photo ) : ?>
                    <figure class="slide-gallery-single">
                        <img src="<?= $photo['photo']; ?>">
                    </figure>
                    <?php endforeach; ?>

                </div>
                <?php endif; ?>

                <div class="portfolio-slide-info">
                    <strong class="portfolio-slide-title"><?= $p_item['title']; ?></strong>
                    <p class="portfolio-slide-place"><?= $p_item['place']; ?></p>

                    <?php if ( $p_item['description'] != 'Описание' && $p_item['description'] != '' ) : ?>
                    <article class="portfolio-slide-text">
                        <p><?= $p_item['description']; ?></p>
                    </article>
                    <?php endif; ?>

                </div>
            </div>
            <?php endforeach; ?>

        </div>
    </div>
</section>
<?php endif; ?>

<?php require_once( 'include-blocks/advantages.php' ); ?>

<section class="block pattern-block intro-testimonials service-testimonials" id="testimonials">
    <div class="container container-small">
        <h3 class="block-title">Начните сотрудничество с командой <br>профессионалов уже сегодня <strong>Прочитайте отзывы о работе с нами</strong></h3>
        <?php
        $indexPageID = get_id_by_slug( 'index' );
        $testimonials = get_field( 'page-reviews', $indexPageID ); ?>

        <?php if ( count( $testimonials ) > 0 ) : ?>
        <div class="testimonials-slider">
            <?php // filter testimonials by type
            $testimonials = array_filter( $testimonials, function($item) {
                if ( $item['type'] == 'Фасадная съемка' ) return $item;
            } ); ?>
            <?php $testimonials = array_chunk( $testimonials, 2 ); ?>

            <?php foreach ( $testimonials as $slide ) : ?>
            <div class="intro-testimonials-slide">

                <?php foreach ( $slide as $testimonial ) : ?>
                <article class="testimonial-single">
                    <p class="testimonial-single-info">
                        <?php if ( $testimonial['city'] ) : ?>
                        <span class="testimonial-single-city"><?= $testimonial['city']; ?></span>
                        <?php endif; ?>
                        <?= $testimonial['author']; ?>
                    </p>
                    <div class="testimonial-single-body">
                        <img src="<?= $testimonial['photo']; ?>" class="testimonial-single-author">
                        <p class="testimonial-single-text"><?= $testimonial['text']; ?></p>
                    </div>
                </article>
                <?php endforeach; ?>

            </div>
            <?php endforeach; ?>

        </div>
        <?php endif; ?>

    </div>
</section>

<?php get_footer(); ?>
