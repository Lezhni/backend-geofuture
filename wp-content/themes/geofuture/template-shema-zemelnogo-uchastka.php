<?php /* Template Name: Схема земельного участка */ ?>

<?php get_header(); ?>

<section class="block intro intro-land-scheme">
    <div class="container">
        <h1 class="intro-title"><?= get_field( 'page-title' ); ?></h1>
        <h2 class="intro-subtitle"><?= get_field( 'page-subtitle' ); ?></h2>
        <h3 class="intro-topography-title intro-land-scheme-title"><?= get_field( 'land-scheme-title' ); ?></h3>
        <p class="intro-description-left"><?= get_field( 'land-scheme-description-left' ); ?></p>
        <div class="intro-callback-form">
            <?= do_shortcode( '[contact-form-7 id="126"]' ); ?>
        </div>
    </div>
</section>

<?php $ls_advantages = get_field( 'advantages' ); ?>
<?php if ( count( $ls_advantages ) > 0 ) : ?>
<section class="block advantages land-scheme-advantages">
    <div class="container">

        <?php foreach( $ls_advantages as $ls_advantage ) : ?>
        <div class="advantage-single">
            <article class="situation-single">
                <strong><?= $ls_advantage['title']; ?></strong>
                <p><?= $ls_advantage['text']; ?></p>
            </article>
        </div>
        <?php endforeach; ?>

    </div>
</section>
<?php endif; ?>

<?php require_once( 'include-blocks/advantages.php' ); ?>

<section class="block intro-testimonials service-testimonials" id="testimonials">
    <div class="container container-small">
        <h3 class="block-title">Начните сотрудничество с командой <br>профессионалов уже сегодня <strong>Прочитайте отзывы о работе с нами</strong></h3>
        <?php
        $indexPageID = get_id_by_slug( 'index' );
        $testimonials = get_field( 'page-reviews', $indexPageID ); ?>

        <?php if ( count( $testimonials ) > 0 ) : ?>
        <div class="testimonials-slider">
            <?php // filter testimonials by type
            $testimonials = array_filter( $testimonials, function($item) {
                if ( $item['type'] == 'Схема земельного участка на КПТ' ) return $item;
            } ); ?>
            <?php $testimonials = array_chunk( $testimonials, 2 ); ?>

            <?php foreach ( $testimonials as $slide ) : ?>
            <div class="intro-testimonials-slide">

                <?php foreach ( $slide as $testimonial ) : ?>
                <article class="testimonial-single">
                    <p class="testimonial-single-info">
                        <?php if ( $testimonial['city'] ) : ?>
                        <span class="testimonial-single-city"><?= $testimonial['city']; ?></span>
                        <?php endif; ?>
                        <?= $testimonial['author']; ?>
                    </p>
                    <div class="testimonial-single-body">
                        <img src="<?= $testimonial['photo']; ?>" class="testimonial-single-author">
                        <p class="testimonial-single-text"><?= $testimonial['text']; ?></p>
                    </div>
                </article>
                <?php endforeach; ?>

            </div>
            <?php endforeach; ?>

        </div>
        <?php endif; ?>

    </div>
</section>

<?php get_footer(); ?>
