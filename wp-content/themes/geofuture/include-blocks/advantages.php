<section class="block dark-block about">
    <div class="container">
        <h3 class="block-title"><strong>КОМПАНИЯ "GEOFUTURE" ЭКСПЕРТ</strong>НА РЫНКЕ В СФЕРЕ ГЕОДЕЗИИ</h3>
        <img src="<?= get_template_directory_uri(); ?>/img/text-geo.png" alt="GEO" class="about-geo">
        <?php
        $indexPageID = get_page_by_path( 'index' );
        $advantages = get_field( 'advantages', $indexPageID );
        ?>
        <div class="about-advantages">
            <div class="about-advantage-single about-advantage-engineers">
                <strong><?= $advantages[0]['text']; ?></strong>
            </div>
            <div class="about-advantage-single about-advantage-equipments">
                <strong><?= $advantages[1]['text']; ?></strong>
            </div>
            <div class="about-advantage-single about-advantage-clients">
                <strong><?= $advantages[2]['text']; ?></strong>
            </div>
            <div class="about-advantage-single about-advantage-expirience">
                <strong><?= $advantages[3]['text']; ?></strong>
            </div>
            <div class="about-advantage-single about-advantage-licenses">
                <strong><?= $advantages[4]['text']; ?></strong>
            </div>
            <div class="about-advantage-single about-advantage-prices">
                <strong><?= $advantages[5]['text']; ?></strong>
            </div>
        </div>
    </div>
</section>
