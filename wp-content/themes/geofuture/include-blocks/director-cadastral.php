<section class="block pattern-block tech-plan-questions">
    <div class="container">
        <h3 class="block-title"><strong>Есть вопросы?</strong>Задайте их начальнику отдела кадастровых работ</h3>
        <div class="tech-plan-questions-info">
            <div class="cadastral-director">
                <img src="<?= get_template_directory_uri(); ?>/img/director-cadastral-photo.png" alt="">
                <a href="#callback" class="button tech-plan-questions-button popup">Задать вопрос</a>
            </div>
            <div class="cadastral-director-info">
                <ul class="cadastral-director-achievements">
                    <li>• Специалист в сфере кадастрового учета</li>
                    <li>• <strong>Образование:</strong> Государственный университет по землеустройству</li>
                    <li>• <strong>Специальность:</strong> Инженер по специальности «Землеустройство»</li>
                    <li>• <strong>Опыт работы:</strong> Работать начала с сентября 2011</li>
                    <li>• <strong>Аттестат КИ:</strong> Апрель 2012</li>
                    <li>• <strong>Профессиональные интересы:</strong> Землеустройство, земельное <br>законодательство, судебная практика</li>
                </ul>
                <blockquote class="cadastral-director-cite">«Мы не боимся решать сложные задачи, ответственно подходим
                к реализации всех объектов и соблюдению сроков»</blockquote>
                <div class="cadastral-director-post">
                    <strong>ЛЕВИНА ЕВГЕНИЯ ВЯЧЕСЛАВОВНА</strong>
                    <small>Начальник отдела кадастровых работ</small>
                </div>
            </div>
        </div>
    </div>
</section>
