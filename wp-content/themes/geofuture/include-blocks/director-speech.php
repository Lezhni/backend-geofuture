<section class="block director-speech">
    <div class="container">
        <article class="director-speech-block">
            <img src="<?= get_template_directory_uri(); ?>/img/director-photo.png" class="director-speech-photo">
            <p class="director-speech-text">
                <q>Мы полностью уверены в своем профессионализме <br>и даем гарантию 7 лет на все выполненные работы.
                <br>Это не пустые слова - это прописано в договоре!</q>
                <strong class="director-speech-name">Генеральный директор компании GeoFuture <span>Павел Максимов</span></strong>
            </p>
        </article>
    </div>
</section>
