<?php get_header(); ?>

<section class="block intro">
    <div class="container">
        <h1 class="intro-title"><?= get_field( 'page-title' ); ?></h1>
        <h2 class="intro-subtitle"><?= get_field( 'page-subtitle' ); ?></h2>
        <a href="#callback" class="intro-button callback-button popup">Заказать обратный звонок</a>
        <div class="index-services-slider">
            <div class="service-slide" data-url="/fasadnaya-semka/">
                <strong class="service-slide-title">Фасадная<br>съемка</strong>
                <img src="<?= get_template_directory_uri(); ?>/img/icon-exterior-survey.png">
            </div>
            <div class="service-slide" data-url="/ispolnitelnaya-semka/">
                <strong class="service-slide-title">Исполнительная<br>съемка</strong>
                <img src="<?= get_template_directory_uri(); ?>/img/icon-executive-survey.png">
            </div>
            <div class="service-slide" data-url="/mezhevanie-uchastkov/">
                <strong class="service-slide-title">Межевание<br>участков</strong>
                <img src="<?= get_template_directory_uri(); ?>/img/icon-survey.png">
            </div>
            <div class="service-slide" data-url="/shema-zemelnogo-uchastka/">
                <strong class="service-slide-title">Схема<br>участка</strong>
                <img src="<?= get_template_directory_uri(); ?>/img/icon-land-scheme.png">
            </div>
            <div class="service-slide" data-url="/soprovozhdenie-stroitelstva/">
                <strong class="service-slide-title">Сопровождение<br>строительства</strong>
                <img src="<?= get_template_directory_uri(); ?>/img/icon-construction-support.png">
            </div>
            <div class="service-slide" data-url="/oformlenie-tehnicheskogo-plana/">
                <strong class="service-slide-title">Оформление<br>тех. плана</strong>
                <img src="<?= get_template_directory_uri(); ?>/img/icon-tech-plan.png">
            </div>
            <div class="service-slide" data-url="/topograficheskie-raboty/">
                <strong class="service-slide-title">Топографические<br>работы</strong>
                <img src="<?= get_template_directory_uri(); ?>/img/icon-topography.png">
            </div>
            <div class="service-slide" data-url="/vynos-granits/">
                <strong class="service-slide-title">Вынос<br>границ</strong>
                <img src="<?= get_template_directory_uri(); ?>/img/icon-borders-takeaway.png">
            </div>
        </div>
    </div>
</section>
<section class="block intro-testimonials" id="testimonials">
    <div class="container container-small">
        <h3 class="block-title">Начните сотрудничество с командой <br>профессионалов уже сегодня <strong>Прочитайте отзывы о работе с нами</strong></h3>
        <nav class="intro-testimonials-navigation">
            <li><a href="#all" class="active">Все отзывы</a></li>
            <li><a href="#exterior-survey">Фасадная съемка</a></li>
            <li><a href="#executive-survey">Исполнительная съемка</a></li>
            <li><a href="#survey">Межевание</a></li>
            <li><a href="#land-scheme">Схема земельного участка на КПТ</a></li>
            <li><a href="#construction-support">Геодезическое сопровождение строительства</a></li>
            <li><a href="#tech-plan">Оформление технического плана</a></li>
            <li><a href="#topography">Топографические работы</a></li>
            <li><a href="#borders-takeaway">Вынос границ земельного участка</a></li>
        </nav>

        <div class="testimonials-sliders-list">
            <?php $testimonialsFull = $testimonials = get_field( 'page-reviews' ); ?>

            <?php if ( count( $testimonials ) > 0 ) : ?>
            <div class="intro-testimonials-slider active" id="all">
                <?php $testimonials = array_chunk( $testimonials, 2 ); ?>

                <?php foreach ( $testimonials as $slide ) : ?>
                <div class="intro-testimonials-slide">

                    <?php foreach ( $slide as $testimonial ) : ?>
                    <article class="testimonial-single">
                        <p class="testimonial-single-info">
                            <?php if ( $testimonial['city'] ) : ?>
                            <span class="testimonial-single-city"><?= $testimonial['city']; ?></span>
                            <?php endif; ?>
                            <?= $testimonial['author']; ?>
                        </p>
                        <div class="testimonial-single-body">
                            <img src="<?= $testimonial['photo']; ?>" class="testimonial-single-author">
                            <p class="testimonial-single-text"><?= $testimonial['text']; ?></p>
                        </div>
                    </article>
                    <?php endforeach; ?>

                </div>
                <?php endforeach; ?>

            </div>
            <nav class="testimonials-slider-navigation active">
                <a href="#" class="change-slide prev-slide">Назад</a>
                <a href="#" class="change-slide next-slide">Далее</a>
            </nav>

            <?php require_once( 'include-sliders/exterior-survey.php' ); ?>
            <?php require_once( 'include-sliders/executive-survey.php' ); ?>
            <?php require_once( 'include-sliders/survey.php' ); ?>
            <?php require_once( 'include-sliders/land-scheme.php' ); ?>
            <?php require_once( 'include-sliders/construction-support.php' ); ?>
            <?php require_once( 'include-sliders/tech-plan.php' ); ?>
            <?php require_once( 'include-sliders/topography.php' ); ?>
            <?php require_once( 'include-sliders/borders-takeaway.php' ); ?>

        <?php endif; ?>

        </div>
    </div>
</section>

<?php require_once( 'include-blocks/advantages.php' ); ?>

<?php get_footer(); ?>
