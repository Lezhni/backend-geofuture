<?php /* Template Name: Топографические работы */ ?>

<?php get_header(); ?>

<div class="modal mfp-hide" id="callback-topography">
    <span class="modal-close">Закрыть</span>
    <?= do_shortcode( '[contact-form-7 id="257"]' ); ?>
</div>

<section class="block intro intro-topography">
    <div class="container">
        <h1 class="intro-title"><?= get_field( 'page-title' ); ?></h1>
        <h2 class="intro-subtitle"><?= get_field( 'page-subtitle' ); ?></h2>
        <h3 class="intro-topography-title"><?= get_field( 'topography-title' ); ?></h3>
        <p class="intro-description-left"><?= get_field( 'topography-description-left' ); ?></p>
        <div class="intro-callback-form">
            <?= do_shortcode( '[contact-form-7 id="164"]' ); ?>
        </div>
    </div>
</section>

<?php $t_types = get_field( 'topography-types' ); ?>
<?php if ( count( $t_types ) > 0 ) : ?>
<section class="block exterior-survey-types typography-types">
    <div class="container">
        <h3 class="block-title">Какие виды топографической сьемки <br>мы предлагаем</h3>
        <section class="typography-types-list services-list">

            <?php foreach ( $t_types as $t_type ) : ?>
            <div class="service-single">
                <article class="service-single-text">
                    <h4 class="service-single-title"><?= $t_type['title']; ?></h4>
                    <p><?= $t_type['text']; ?></p>
                </article>
                <a href="#callback-topography" class="button service-single-order popup">Получить расчет стоимости</a>
            </div>
        <?php endforeach; ?>

        </section>
    </div>
</section>
<?php endif; ?>

<?php $portfolio = get_field( 'portfolio' ); ?>
<?php if ( count( $portfolio ) > 0 ) : ?>
<section class="block pattern-block exterior-portfolio portfolio">
    <div class="container">
        <h3 class="block-title">Посмотрите примеры работ, <br>которые мы делаем</h3>
        <div class="portfolio-slider">

            <?php foreach ( $portfolio as $p_item ) : ?>
            <div class="portfolio-slide">

                <?php if ( count( $p_item['photos'] ) > 0 ) : ?>
                <div class="portfolio-slide-gallery">

                    <?php foreach ( $p_item['photos'] as $photo ) : ?>
                    <figure class="slide-gallery-single">
                        <img src="<?= $photo['photo']; ?>">
                    </figure>
                    <?php endforeach; ?>

                </div>
                <?php endif; ?>

                <div class="portfolio-slide-info">
                    <strong class="portfolio-slide-title"><?= $p_item['title']; ?></strong>
                    <p class="portfolio-slide-place"><?= $p_item['place']; ?></p>

                    <?php if ( $p_item['description'] != 'Описание' && $p_item['description'] != '' ) : ?>
                    <article class="portfolio-slide-text">
                        <p><?= $p_item['description']; ?></p>
                    </article>
                    <?php endif; ?>

                </div>
            </div>
            <?php endforeach; ?>

        </div>
    </div>
</section>
<?php endif; ?>

<?php require_once( 'include-blocks/advantages.php' ); ?>

<section class="block intro-testimonials service-testimonials" id="testimonials">
    <div class="container container-small">
        <h3 class="block-title">Начните сотрудничество с командой <br>профессионалов уже сегодня <strong>Прочитайте отзывы о работе с нами</strong></h3>
        <?php
        $indexPageID = get_id_by_slug( 'index' );
        $testimonials = get_field( 'page-reviews', $indexPageID ); ?>

        <?php if ( count( $testimonials ) > 0 ) : ?>
        <div class="testimonials-slider">
            <?php // filter testimonials by type
            $testimonials = array_filter( $testimonials, function($item) {
                if ( $item['type'] == 'Топографические работы' ) return $item;
            } ); ?>
            <?php $testimonials = array_chunk( $testimonials, 2 ); ?>

            <?php foreach ( $testimonials as $slide ) : ?>
            <div class="intro-testimonials-slide">

                <?php foreach ( $slide as $testimonial ) : ?>
                <article class="testimonial-single">
                    <p class="testimonial-single-info">
                        <?php if ( $testimonial['city'] ) : ?>
                        <span class="testimonial-single-city"><?= $testimonial['city']; ?></span>
                        <?php endif; ?>
                        <?= $testimonial['author']; ?>
                    </p>
                    <div class="testimonial-single-body">
                        <img src="<?= $testimonial['photo']; ?>" class="testimonial-single-author">
                        <p class="testimonial-single-text"><?= $testimonial['text']; ?></p>
                    </div>
                </article>
                <?php endforeach; ?>

            </div>
            <?php endforeach; ?>

        </div>
        <?php endif; ?>

    </div>
</section>

<?php $cl_advantages = get_field( 'clients-advantages' ); ?>
<?php if ( count( $cl_advantages ) > 0 ) : ?>
<section class="block clients-advantages">
    <div class="container">
        <h3 class="block-title">Что получают заказчики, заказывая <br>наши услуги</h3>
    </div>
    <?php
        $cl_top = array_slice( $cl_advantages, 0, 3 );
        $cl_bottom = array_slice( $cl_advantages, 2, 3 );
    ?>
    <div class="clients-advantages-list">

        <?php if ( count ($cl_top ) > 0 ) : ?>
        <div class="clients-advantages-top">
            <div class="container">

                <?php foreach ( $cl_top as $cl_advantage ) : ?>
                <article class="clients-advantage-single">
                    <p><?= $cl_advantage['text']; ?></p>
                </article>
                <?php endforeach; ?>

            </div>
        </div>
        <?php endif; ?>

        <?php if ( count( $cl_bottom ) > 0 ) : ?>
        <div class="clients-advantages-bottom">
            <div class="container">

                <?php foreach ( $cl_bottom as $cl_advantage ) : ?>
                <article class="clients-advantage-single">
                    <p><?= $cl_advantage['text']; ?></p>
                </article>
                <?php endforeach; ?>

            </div>
        </div>
        <?php endif; ?>

    </div>
</section>
<?php endif; ?>

<?php $recommendations = get_field( 'recommendations' ); ?>
<?php if ( count( $recommendations ) > 0 ) : ?>
<section class="block pattern-block recommendations">
    <div class="container">
        <h3 class="block-title">ПОЧЕМУ 62% НАШИХ КЛИЕНТОВ <br>ПРИХОДЯТ К НАМ ПО РЕКОМЕНДАЦИИ</h3>
        <div class="recommendations-list">

            <?php foreach ( $recommendations as $recommendation ) : ?>
            <article class="recommendation-single">
                <p><?= $recommendation['text']; ?></p>
            </article>
            <?php endforeach; ?>

        </div>
    </div>
</section>
<?php endif; ?>

<?php require_once( 'include-blocks/director-speech.php' ); ?>

<?php get_footer(); ?>
