<?php /* Template Name: Исполнительная съемка */ ?>

<?php get_header(); ?>

<section class="block intro intro-executive-survey">
    <div class="container">
        <h1 class="intro-title"><?= get_field( 'page-title'); ?></h1>
        <h2 class="intro-subtitle"><?= get_field( 'page-subtitle'); ?></h2>
        <p class="intro-description-left">
            <?= get_field( 'ispol-intro-text' ); ?>
        </p>
        <div class="intro-callback-form">
            <?= do_shortcode( '[contact-form-7 id="43"]' ); ?>
        </div>
    </div>
</section>

<?php $advantages = get_field( 'ispol-advantages' ); ?>

<?php if ( count( $advantages ) > 0 ) : ?>
<section class="block executive-survey-advantages advantages">
    <div class="container">

        <?php foreach ( $advantages as $advantage ) : ?>
        <div class="advantage-single">
            <article class="situation-single">
                <p><?= $advantage['text']; ?></p>
            </article>
        </div>
        <?php endforeach; ?>

    </div>
</section>
<?php endif; ?>

<?php $portfolio = get_field( 'portfolio' ); ?>
<?php if ( count( $portfolio ) > 0 ) : ?>
<section class="block pattern-block exterior-portfolio portfolio">
    <div class="container">
        <h3 class="block-title">Посмотрите примеры работ, <br>которые мы делаем</h3>
        <div class="portfolio-slider">

            <?php foreach ( $portfolio as $p_item ) : ?>
            <div class="portfolio-slide">

                <?php if ( count( $p_item['photos'] ) > 0 ) : ?>
                <div class="portfolio-slide-gallery">

                    <?php foreach ( $p_item['photos'] as $photo ) : ?>
                    <figure class="slide-gallery-single">
                        <img src="<?= $photo['photo']; ?>">
                    </figure>
                    <?php endforeach; ?>

                </div>
                <?php endif; ?>

                <div class="portfolio-slide-info">
                    <strong class="portfolio-slide-title"><?= $p_item['title']; ?></strong>
                    <p class="portfolio-slide-place"><?= $p_item['place']; ?></p>

                    <?php if ( $p_item['description'] != 'Описание' && $p_item['description'] != '' ) : ?>
                    <article class="portfolio-slide-text">
                        <p><?= $p_item['description']; ?></p>
                    </article>
                    <?php endif; ?>

                </div>
            </div>
            <?php endforeach; ?>

        </div>
    </div>
</section>
<?php endif; ?>

<section class="block intro-testimonials service-testimonials" id="testimonials">
    <div class="container container-small">
        <h3 class="block-title">Начните сотрудничество с командой <br>профессионалов уже сегодня <strong>Прочитайте отзывы о работе с нами</strong></h3>
        <?php
        $indexPageID = get_id_by_slug( 'index' );
        $testimonials = get_field( 'page-reviews', $indexPageID ); ?>

        <?php if ( count( $testimonials ) > 0 ) : ?>
        <div class="testimonials-slider">
            <?php // filter testimonials by type
            $testimonials = array_filter( $testimonials, function($item) {
                if ( $item['type'] == 'Исполнительная съемка' ) return $item;
            } ); ?>
            <?php $testimonials = array_chunk( $testimonials, 2 ); ?>

            <?php foreach ( $testimonials as $slide ) : ?>
            <div class="intro-testimonials-slide">

                <?php foreach ( $slide as $testimonial ) : ?>
                <article class="testimonial-single">
                    <p class="testimonial-single-info">
                        <?php if ( $testimonial['city'] ) : ?>
                        <span class="testimonial-single-city"><?= $testimonial['city']; ?></span>
                        <?php endif; ?>
                        <?= $testimonial['author']; ?>
                    </p>
                    <div class="testimonial-single-body">
                        <img src="<?= $testimonial['photo']; ?>" class="testimonial-single-author">
                        <p class="testimonial-single-text"><?= $testimonial['text']; ?></p>
                    </div>
                </article>
                <?php endforeach; ?>

            </div>
            <?php endforeach; ?>

        </div>
        <?php endif; ?>

    </div>
</section>

<?php require_once( 'include-blocks/advantages.php' ); ?>

<?php $documents = get_field( 'documents' ); ?>
<?php if ( count( $documents ) > 0 ) : ?>
<section class="block executive-survey-documents documents">
    <div class="container">
        <h3 class="block-title">Документы, подтверждающие <br> высокий уровень компании</h3>
        <div class="documents-list">
            <ul>

                <?php foreach ( $documents as $document ) : ?>
                <li class="document-single">
                    <a href="<?= $document['photo']; ?>" class="document-single-view"></a>
                    <strong class="document-single-title"><?= $document['title']; ?></strong>
                    <img src="<?= $document['photo']; ?>">
                </li>
                <?php endforeach; ?>

            </ul>
        </div>
    </div>
</section>
<?php endif; ?>

<?php get_footer(); ?>
