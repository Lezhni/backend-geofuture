<?php

// register necessary js and css
function styling() {

    wp_enqueue_style( 'main', get_template_directory_uri() . '/css/style.min.css' );

    //wp_deregister_script( 'jquery' );
    //wp_register_script( 'jquery', 'https://code.jquery.com/jquery-1.12.4.min.js', array(), false, true );
    wp_register_script( 'yandex-map', '//api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU', array(), false, true);
    wp_register_script( 'main', get_template_directory_uri() . '/js/main.min.js', array( 'jquery' ), false, true );

    wp_enqueue_script( 'jquery' );
    wp_enqueue_script( 'yandex-map' );
    wp_enqueue_script( 'main' );
}
add_action( 'wp_enqueue_scripts', 'styling' );

// register top menu
function register_menu() {
  register_nav_menu( 'header-menu', __( 'Верхнее меню' ) );
}
add_action( 'init', 'register_menu' );

// add fields to general options page
function register_fields() {
    $settingFields = array(
        'phone' => 'Контактный телефон',
        'email' => 'Контактный email',
        'address' => 'Контактный адрес',
        'vk' => 'Страница ВКонтакте',
        'fb' => 'Страница Facebook',
        'ig' => 'Страница Instagram'
    );

    add_settings_section(
		'site_contacts',
		'Контактная информация',
		'useless_callback',
        'general'
	);

    foreach ( $settingFields as $field => $label ) {
        register_setting( 'general', 'site_' . $field, 'esc_attr' );
        add_settings_field(
    		'site_' . $field,
    		'<label for="site_' . $field . '">' . __( $label , 'geofuture_theme' ) . '</label>',
            function() use ($field) {
                $value = get_option( 'site_' . $field, '' );
                echo '<input type="text" id="site_' . $field . '" name="site_' . $field . '" value="' . esc_attr( $value ) . '" />';
            },
    		'general',
            'site_contacts'
    	);
    }


}
function useless_callback() {
    return true; // this callback required but useless
}
add_action( 'admin_init', 'register_fields' );

// remove useless emoji
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

// remove admin bar
function remove_admin_bar() {
  show_admin_bar( false );
}
add_action( 'after_setup_theme', 'remove_admin_bar' );

// remove useless page types
function its_not_a_blog() {
    remove_menu_page( 'edit.php' );
    remove_menu_page( 'edit-comments.php' );
}
add_action( 'admin_init', 'its_not_a_blog' );

// remove content editor (not uses)
function bye_bye_wysiwyg() {
    remove_post_type_support( 'page', 'editor' );
}
add_action( 'admin_init', 'bye_bye_wysiwyg' );

// get page id by slug
function get_id_by_slug($page_slug) {
	$page = get_page_by_path( $page_slug );
	if ( $page ) {
		return $page->ID;
	} else {
		return null;
	}
}
