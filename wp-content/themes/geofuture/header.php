<!doctype html>
<!--[if IE 8]>    <html class="no-js ie8 oldie" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> prefix="http://ogp.me/ns#"> <!--<![endif]-->
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="theme-color" content="#001a31">
    <!--[if lt IE 9]>
  	<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
  	<![endif]-->
    <link rel="shortcut icon" href="/favicon.ico">
    <title><?php wp_title(); ?></title>

    <?php wp_head(); ?>

</head>
<body <?php body_class(); ?> >

    <div class="modal mfp-hide" id="callback">
        <span class="modal-close">Закрыть</span>
        <?= do_shortcode( '[contact-form-7 id="11"]' ); ?>
    </div>

    <div class="modal mfp-hide" id="thanks">
        <span class="modal-close">Закрыть</span>
        <h3 class="modal-title"><strong>Спасибо за заявку !</strong></h3>
        <p class="modal-subtitle">Наш менеджер свяжется с Вами <br>в ближайшее время</p>
    </div>

    <aside class="smart-panel">
        <span class="smart-panel-close"></span>
        <nav class="smart-panel-menu">
            <?php wp_nav_menu( array(
                'theme_location' => 'header-menu'
            ) ); ?>
        </nav>
    </aside>
    <header role="banner">
	    <div class="container">
	        <a href="<?= bloginfo( 'url' ); ?>" class="header-logo">
	            <img src="<?= get_template_directory_uri(); ?>/img/logo.png" alt="<?= bloginfo( 'description' ); ?>">
	        </a>
            <nav class="header-menu">
                <?php wp_nav_menu( array(
                    'theme_location' => 'header-menu'
                ) ); ?>
            </nav>
            <div class="header-contact">
                <?php $formatedPhone = preg_replace( '/[^0-9.\+]+/', '', get_option( 'site_phone' ) ); ?>
                <a href="tel:<?= $formatedPhone; ?>" class="header-contact-phone"><?= get_option( 'site_phone' ); ?></a>
                <small class="header-contact-desc">Есть вопрос? Звоните &ndash; поможем!</small>
            </div>
            <span class="header-burger">
                <xml version="1.0" encoding="iso-8859-1">
                <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="40px" height="40px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve">
                <g>
                	<g>
                		<circle cx="69.545" cy="306" r="69.545" fill="#FFFFFF"/>
                		<circle cx="306" cy="306" r="69.545" fill="#FFFFFF"/>
                		<circle cx="542.455" cy="306" r="69.545" fill="#FFFFFF"/>
                	</g>
                </g>
                </svg>
            </span>
	    </div>
	</header>
