<?php /* Template Name: Вынос границ */ ?>

<?php get_header(); ?>

<div class="modal mfp-hide" id="callback-question">
    <span class="modal-close">Закрыть</span>
    <?= do_shortcode( '[contact-form-7 id="258"]' ); ?>
</div>

<section class="block intro intro-land-scheme intro-borders-takeaway">
    <div class="container">
        <h1 class="intro-title"><?= get_field( 'page-title' ); ?></h1>
        <h2 class="intro-subtitle"><?= get_field( 'page-subtitle' ); ?></h2>
        <h3 class="intro-topography-title intro-land-scheme-title"><?= get_field( 'borders-takeaway-title' ); ?></h3>
        <p class="intro-description-left"><?= get_field( 'borders-takeaway-description-title' ); ?></p>
        <div class="intro-callback-form">
            <?= do_shortcode( '[contact-form-7 id="167"]' ); ?>
        </div>
    </div>
</section>
<section class="block intro-testimonials service-testimonials" id="testimonials">
    <div class="container container-small">
        <h3 class="block-title">Начните сотрудничество с командой <br>профессионалов уже сегодня <strong>Прочитайте отзывы о работе с нами</strong></h3>
        <?php
        $indexPageID = get_id_by_slug( 'index' );
        $testimonials = get_field( 'page-reviews', $indexPageID ); ?>

        <?php if ( count( $testimonials ) > 0 ) : ?>
        <div class="testimonials-slider">
            <?php // filter testimonials by type
            $testimonials = array_filter( $testimonials, function($item) {
                if ( $item['type'] == 'Вынос границ земельного участка' ) return $item;
            } ); ?>
            <?php $testimonials = array_chunk( $testimonials, 2 ); ?>

            <?php foreach ( $testimonials as $slide ) : ?>
            <div class="intro-testimonials-slide">

                <?php foreach ( $slide as $testimonial ) : ?>
                <article class="testimonial-single">
                    <p class="testimonial-single-info">
                        <?php if ( $testimonial['city'] ) : ?>
                        <span class="testimonial-single-city"><?= $testimonial['city']; ?></span>
                        <?php endif; ?>
                        <?= $testimonial['author']; ?>
                    </p>
                    <div class="testimonial-single-body">
                        <img src="<?= $testimonial['photo']; ?>" class="testimonial-single-author">
                        <p class="testimonial-single-text"><?= $testimonial['text']; ?></p>
                    </div>
                </article>
                <?php endforeach; ?>

            </div>
            <?php endforeach; ?>

        </div>
        <?php endif; ?>

    </div>
</section>

<?php $documents = get_field( 'documents' ); ?>
<?php if ( count( $documents ) > 0 ) : ?>
<section class="block pattern-block construction-support-documents documents">
    <div class="container">
        <h3 class="block-title">После окончания работ вы получите <strong>следующие документы</strong></h3>
        <div class="documents-list without-slider">
            <ul>

                <?php foreach ( $documents as $document ) : ?>
                <li class="document-single">
                    <a href="<?= $document['photo']; ?>" class="document-single-view"></a>
                    <strong class="document-single-title"><?= $document['title']; ?></strong>
                    <img src="<?= $document['photo']; ?>">
                </li>
                <?php endforeach; ?>

            </ul>
        </div>
        <a href="#callback" class="button documents-list-button popup">Заказать вынос кольев сегодня</a>
    </div>
</section>
<?php endif; ?>

<?php require_once( 'include-blocks/advantages.php' ); ?>

<?php $steps = get_field( 'steps' ); ?>
<?php if ( count( $steps ) > 0 ) : ?>
<section class="block borders-takeaway-steps">
    <div class="container">
        <h3 class="block-title">Процесс выноса межевых знаков</h3>
    </div>
    <div class="borders-takeaway-steps-container advantages">
        <div class="container">

            <?php foreach ( $steps as $step ) : ?>
            <div class="advantage-single">
                <article class="situation-single">
                    <strong><?= $step['title']; ?></strong>
                    <p><?= $step['text']; ?></p>
                </article>
            </div>
            <?php endforeach; ?>

        </div>
    </div>
</section>
<?php endif; ?>

<section class="block borders-takeaway-call">
    <div class="container">
        <?php $formatedPhone = preg_replace( '/[^0-9.\+]+/', '', get_option( 'site_phone' ) ); ?>
        <h3 class="block-title"><strong>ПРОСТО ПОЗВОНИТЕ НАМ <a href="call:<?= $formatedPhone; ?>"><?= get_option( 'site_phone' ); ?></a></strong> И ОЦЕНИТЕ КАЧЕСТВО НАШЕГО СЕРВИСА САМИ</h3>
        <article class="borders-takeaway-call-text">
            <p>Или закажите звонок, и мы перезвоним Вам в течение 7 минут</p>
        </article>
        <a href="#callback" class="callback-button popup">Заказать обратный звонок</a>
    </div>
</section>
<section class="block pattern-block need-answer">
    <div class="container">
        <h3 class="block-title"><strong>Остались вопросы?</strong> Задайте их нашему инженеру первого класса!</h3>
        <a href="#callback-question" class="button button-white popup">Задать вопрос</a>
    </div>
</section>

<?php get_footer(); ?>
