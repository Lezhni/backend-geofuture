    <section class="block contacts" id="contacts">
        <div class="dark-block contacts-info">
            <div class="contacts-info-inner">
                <address class="contacts-location"><?= get_option( 'site_address' ); ?></address>
                <?php $formatedPhone = preg_replace( '/[^0-9.\+]+/', '', get_option( 'site_phone' ) ); ?>
                <a href="tel:<?= $formatedPhone; ?>" class="contacts-phone"><?= get_option( 'site_phone' ); ?></a>
                <a href="mailto:<?= get_option( 'site_email' ); ?>" class="contacts-email"><?= get_option( 'site_email' ); ?></a>
                <nav class="contacts-social">
                    <a href="<?= get_option( 'site_vk' ); ?>" target="_blank"><img src="<?= get_template_directory_uri(); ?>/img/icon-vk.png" alt="vk"></a>
                    <a href="<?= get_option( 'site_fb' ); ?>" target="_blank"><img src="<?= get_template_directory_uri(); ?>/img/icon-instagram.png" alt="instagram"></a>
                    <a href="<?= get_option( 'site_ig' ); ?>" target="_blank"><img src="<?= get_template_directory_uri(); ?>/img/icon-facebook.png" alt="facebook"></a>
                </nav>
                <a href="#callback" class="contacts-button callback-button popup">Заказать обратный звонок</a>
            </div>
        </div>
        <div class="contacts-map" id="contacts-map"></div>
    </section>
    <footer role="contentinfo">
        <div class="container">
            <a href="<?= bloginfo( 'url' ); ?>" class="footer-logo">
                <img src="<?= get_template_directory_uri(); ?>/img/logo-color.png" alt="GeoFuture">
                <span>Геодезическая компания <strong>"GeoFuture"</strong></span>
            </a>
            <a href="http://marketingtime.ru" target="_blank" class="marketing-time-link">MarketingTime <small>создание продающих сайтов</small></a>
            <div class="footer-contact">
                <a href="tel:<?= $formatedPhone; ?>" class="footer-contact-phone"><?= get_option( 'site_phone' ); ?></a>
                <small class="footer-contact-desc">Ежедневно с 10:00 до 19:00</small>
            </div>
        </div>
    </footer>

    <?php wp_footer(); ?>

</body>
</html>
