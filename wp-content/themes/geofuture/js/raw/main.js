$ = jQuery;

var map, placemark, address, query, pos;
var timer;
var testimonialsSliderSettings = {
    dots: true,
    arrows: false,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    autoplay: true,
    autoplaySpeed: 6000
};
var testimonialsIndexSliderSettings = {
    dots: false,
    arrows: false,
    infinite: true,
    speed: 300,
    slidesToShow: 1
};

$(document).on('ready', init);
$(window).on('load', loaded);
$(window).on('scroll', scrolling);

function init() {

    changeHeader();

    // init map in contacts
    ymaps.ready(function(){

        address = "г.Москва, ул.Угрешская, д.2 стр.1";
        query = "https://geocode-maps.yandex.ru/1.x/?format=json&geocode=" + address;

        // try to get coords from localStorage
        pos = localStorage.getItem('coords');

        if (pos) {
            drawMap();
        } else {
            // if coords not cached in localStorage
            $.get(query, function(data) {
                pos = data.response.GeoObjectCollection.featureMember[0].GeoObject.Point.pos;
                localStorage.setItem('coords', pos);

                drawMap();
            });
        }
    });

    // initialize ionRangeSlider
    $('.range-slider').ionRangeSlider({
        min: 3,
        max: 99,
        from: 5,
        hide_min_max: true
    });

    // portfolio sliders
    $('.portfolio-slider').slick({
        arrows: true,
        prevArrow: '<span class="slider-arrow prev-arrow">Назад</span>',
        nextArrow: '<span class="slider-arrow next-arrow">Далее</span>',
        infinite: true,
        speed: 300,
        slidesToShow: 1
    });

    $('.index-services-slider').slick({
        arrows: true,
        dots: false,
        centerMode: true,
        slidesToShow: 5,
        prevArrow: '<span class="change-services-slide prev-slide"></span>',
        nextArrow: '<span class="change-services-slide next-slide"></span>',
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                    touchMove: false
                }
            }
        ]
    });

    $('.intro-testimonials-slider').slick(testimonialsIndexSliderSettings);

    $('.portfolio-slide-gallery:not(.single-photo)').slick({
        dots: true,
        arrows: false,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        autoplay: true,
        autoplaySpeed: 3000,
        vertical: true,
        verticalSwiping: true
    });

    $('.survey-types-slider').slick({
        dots: true,
        arrows: false,
        infinite: true,
        speed: 300,
        slidesToShow: 1
    });

    $('.testimonials-slider').slick(testimonialsSliderSettings);

    $('.documents-list:not(.without-slider)').jScrollPane();

    $('input[type="tel"]').inputmask({
  	    mask: '+7 (999) 999-99-99',
  	    showMaskOnHover: false,
  	    showMaskOnFocus: true,
  	});

    $('.documents-list').magnificPopup({
		delegate: '.document-single a',
		type: 'image',
		tLoading: 'Загрузка изображения #%curr%...',
		mainClass: 'mfp-img-mobile',
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1]
		},
		image: {
			tError: '<a href="%url%">Изображение #%curr%</a> не может быть загружено.'
		}
	});
}

function scrolling() {

    changeHeader();
}

function loaded() {
}

// events

$('.popup').click(function() {

    var target = $(this).attr('href');
    if (target == '' && target == '#') {
        return false;
    }

    $.magnificPopup.open({
        items: {
            src: target
        },
        type: 'inline',
        showCloseBtn: false
    });

    return false;
});

$('.header-burger, .smart-panel-close').click(function() {

    $('body, html').toggleClass('panel-opened');
});

$('.header-menu a, .smart-panel-menu a').click(function() {
    var target = $(this).attr('href');
    if ($(window).width() <= 768) {
        var headerHeight = 75;
    } else {
        var headerHeight = 100;
    }

    if (target == '#contacts' || target == '#testimonials') {
        $('html, body').animate({
            scrollTop: $(target).offset().top - headerHeight
        });

        return false;
    }
});

$(document).on('click', '.modal-close', function() {
    $.magnificPopup.close();
});

$('.change-slide').click(function() {

    if ($(this).hasClass('prev-slide')) {
        $('.intro-testimonials-slider').slick('slickPrev');
    } else {
        $('.intro-testimonials-slider').slick('slickNext');
    }

    return false;
});

$('.change-services-slide').click(function() {

    if ($(this).hasClass('prev-slide')) {
        $('.index-services-slider').slick('slickPrev');
    } else {
        $('.index-services-slider').slick('slickNext');
    }

    return false;
});

// $('.index-services-slider').on('afterChange', function(e, slick, currentSlide) {
//
//     if ($(window).innerWidth() > 768) return false;
//
//     var url = $('.service-slide[data-slick-index=' + currentSlide + ']').attr('data-url');
//     var service = $('.service-slide[data-slick-index=' + currentSlide + ']').find('.service-slide-title').html();
//     service = service.replace('<br>', ' ');
//
//     timer = setTimeout(function() {
//         var answer = confirm('Желаете ли перейти к услуге "' + service + '"?');
//         if (answer) {
//             window.location.href = url;
//         }
//     }, 1000);
// });

$('.index-services-slider').on('beforeChange', function(e, slick, currentSlide, nextSlide) {

    if ($(window).innerWidth() > 768) return false;

    clearTimeout(timer);
});

$('.service-slide img').on('click', function() {

    if ($(window).innerWidth() <= 768) {
        var url = $(this).closest('.service-slide').attr('data-url');
        var service = $(this).closest('.service-slide').find('.service-slide-title').html();
        service = service.replace('<br>', ' ');

        timer = setTimeout(function() {
            var answer = confirm('Желаете ли перейти к услуге "' + service + '"?');
            if (answer) {
                window.location.href = url;
            }
        }, 1000);
    } else {
        var url = $(this).closest('.service-slide').attr('data-url');
        window.location.href = url;
    }
});

$('.intro-testimonials-navigation a').click(function() {

    if (!$(this).hasClass('active')) {
        var target = $(this).attr('href');
        var settings;
        if (target == '#all') {
            settings = testimonialsIndexSliderSettings;
        } else {
            settings = testimonialsSliderSettings;
        }

        $('.intro-testimonials-navigation a').removeClass('active');
        $(this).addClass('active');

        $('.intro-testimonials-slider, .testimonials-slider-navigation, .testimonials-sliders-list .testimonials-slider').removeClass('active');
        $('.testimonials-sliders-list ' + target)
            .addClass('active')
            .promise()
            .done(function() {
                console.log(target);
                $('.testimonials-sliders-list ' + target).slick('unslick');
                $('.testimonials-sliders-list ' + target).slick(settings);

            });

        if (target == '#all') {
            $('.testimonials-slider-navigation').addClass('active');
        }
    }

    return false;
});

$('.service-slide img').hover(function() {
    var white = $(this).attr('data-white-icon');
    $(this).attr('src', white);
}, function() {
    var red = $(this).attr('data-icon');
    $(this).attr('src', red);
});

$('.intro-testimonials-slider').on('init', function(e, slick) {

    var minSlideHeight = $('.intro-testimonials-slider .intro-testimonials-slide').eq(0).next().height();
    var maxSlideHeight = $('.intro-testimonials-slider').height();
    $('.testimonials-slider-navigation').css('transform', 'translateY(-' + (maxSlideHeight - minSlideHeight) + 'px)');
});

$('.testimonials-slider').on('init', function(e, slick) {

    minSlideHeight = $(this).find('.intro-testimonials-slide').eq(0).next().height();
    maxSlideHeight = $(this).height();
    $(this).find('.slick-dots').css('transform', 'translateY(-' + (maxSlideHeight - minSlideHeight - 70) + 'px)');
});

$('.intro-testimonials-slider').on('afterChange', function(e, slick, currentSlide) {

    var minSlideHeight = $('.intro-testimonials-slider .intro-testimonials-slide').eq(currentSlide).next().height();
    var maxSlideHeight = $('.intro-testimonials-slider').height();
    $('.testimonials-slider-navigation').css('transform', 'translateY(-' + (maxSlideHeight - minSlideHeight) + 'px)');
});

$('.testimonials-slider').on('afterChange', function(e, slick, currentSlide) {

    var minSlideHeight = $(this).find('.intro-testimonials-slide').eq(currentSlide).next().height();
    var maxSlideHeight = $(this).height();
    $(this).find('.slick-dots').css('transform', 'translateY(-' + (maxSlideHeight - minSlideHeight - 70) + 'px)');
});

// callback functions
function drawMap() {

    pos = pos.split(' ').reverse();

    map = new ymaps.Map("contacts-map", {
        center: pos,
        zoom: 13,
    }, {
        suppressMapOpenBlock: true
    });

    map.controls.add('zoomControl');

    placemark = new ymaps.Placemark(pos, {
        hintContent: address
    }, {
        iconLayout: "default#image",
        iconImageHref: '/wp-content/themes/geofuture/img/icon-map-marker.png',
        iconImageSize: [48, 48],
        iconImageOffset: [-24, -24]
    });
    map.geoObjects.add(placemark);
}

// change header from absolute to fixed (fixed to absolute)
function changeHeader() {

    var headerHeight = $('.intro').innerHeight();
    if ($('body').scrollTop() >= headerHeight) {
        $('header').addClass('fixed');
    } else {
        $('header').removeClass('fixed');
    }
}
