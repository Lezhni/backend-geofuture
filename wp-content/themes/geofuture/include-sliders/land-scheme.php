<!-- Схема земельного участка на КПТ -->
<div class="testimonials-slider" id="land-scheme">
    <?php // filter testimonials by type
    $testimonials = array_filter( $testimonialsFull, function($item) {
        if ( $item['type'] == 'Схема земельного участка на КПТ' ) return $item;
    } ); ?>
    <?php $testimonials = array_chunk( $testimonials, 2 ); ?>

    <?php foreach ( $testimonials as $slide ) : ?>
    <div class="intro-testimonials-slide">

        <?php foreach ( $slide as $testimonial ) : ?>
        <article class="testimonial-single">
            <p class="testimonial-single-info">
                <?php if ( $testimonial['city'] ) : ?>
                <span class="testimonial-single-city"><?= $testimonial['city']; ?></span>
                <?php endif; ?>
                <?= $testimonial['author']; ?>
            </p>
            <div class="testimonial-single-body">
                <img src="<?= $testimonial['photo']; ?>" class="testimonial-single-author">
                <p class="testimonial-single-text"><?= $testimonial['text']; ?></p>
            </div>
        </article>
        <?php endforeach; ?>

    </div>
    <?php endforeach; ?>

</div>
