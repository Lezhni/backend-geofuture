<?php /* Template Name: Оформление технического плана */ ?>

<?php get_header(); ?>

<section class="block intro intro-tech-plan">
    <div class="container">
        <h1 class="intro-title"><?= get_field( 'page-title' ); ?></h1>
        <h2 class="intro-subtitle"><?= get_field( 'page-subtitle' ); ?></h2>
        <h3 class="intro-topography-title intro-tech-plan-title"><?= get_field( 'tech-plan-title' ); ?></h3>
        <p class="intro-description-left"><?= get_field( 'tech-plan-description-left' ); ?></p>
        <div class="intro-callback-form">
            <?= do_shortcode( '[contact-form-7 id="151"]' ); ?>
        </div>
    </div>
</section>

<?php $tp_reasons = get_field( 'tech-plan-reasons' ); ?>
<?php if ( count( $tp_reasons ) > 0 ) : ?>
<section class="block advantages situations tech-plan-situations">
    <div class="container">
        <div class="advantage-single without-counter">
            <article class="situation-single">
                <h3 class="block-title">
                    В КАКИХ СЛУЧАЯХ НЕОБХОДИМ
                    <span>ТЕХНИЧЕСКИЙ ПЛАН и</span>
                    <span>КАДАСТРОВЫЙ ПАСПОРТ НА ЗДАНИЕ</span>
                </h3>
            </article>
        </div>

        <?php foreach ( $tp_reasons as $tp_reason ) : ?>
        <div class="advantage-single">
            <article class="situation-single">
                <p><?= $tp_reason['text']; ?></p>
            </article>
        </div>
        <?php endforeach; ?>

    </div>
</section>
<?php endif; ?>

<?php require_once( 'include-blocks/advantages.php' ); ?>

<section class="block pattern-block intro-testimonials service-testimonials" id="testimonials">
    <div class="container container-small">
        <h3 class="block-title">Начните сотрудничество с командой <br>профессионалов уже сегодня <strong>Прочитайте отзывы о работе с нами</strong></h3>
        <?php
        $indexPageID = get_id_by_slug( 'index' );
        $testimonials = get_field( 'page-reviews', $indexPageID ); ?>

        <?php if ( count( $testimonials ) > 0 ) : ?>
        <div class="testimonials-slider">
            <?php // filter testimonials by type
            $testimonials = array_filter( $testimonials, function($item) {
                if ( $item['type'] == 'Оформление технического плана' ) return $item;
            } ); ?>
            <?php $testimonials = array_chunk( $testimonials, 2 ); ?>

            <?php foreach ( $testimonials as $slide ) : ?>
            <div class="intro-testimonials-slide">

                <?php foreach ( $slide as $testimonial ) : ?>
                <article class="testimonial-single">
                    <p class="testimonial-single-info">
                        <?php if ( $testimonial['city'] ) : ?>
                        <span class="testimonial-single-city"><?= $testimonial['city']; ?></span>
                        <?php endif; ?>
                        <?= $testimonial['author']; ?>
                    </p>
                    <div class="testimonial-single-body">
                        <img src="<?= $testimonial['photo']; ?>" class="testimonial-single-author">
                        <p class="testimonial-single-text"><?= $testimonial['text']; ?></p>
                    </div>
                </article>
                <?php endforeach; ?>

            </div>
            <?php endforeach; ?>

        </div>
        <?php endif; ?>

    </div>
</section>

<?php $documents = get_field( 'documents' ); ?>
<?php if ( count( $documents ) > 0 ) : ?>
<section class="block executive-survey-documents documents">
    <div class="container">
        <h3 class="block-title">Документы, подтверждающие <br> высокий уровень компании</h3>
        <div class="documents-list">
            <ul>

                <?php foreach ( $documents as $document ) : ?>
                <li class="document-single">
                    <a href="<?= $document['photo']; ?>" class="document-single-view"></a>
                    <strong class="document-single-title"><?= $document['title']; ?></strong>
                    <img src="<?= $document['photo']; ?>">
                </li>
                <?php endforeach; ?>

            </ul>
        </div>
    </div>
</section>
<?php endif; ?>

<?php require_once( 'include-blocks/director-cadastral.php' ); ?>

<?php get_footer(); ?>
