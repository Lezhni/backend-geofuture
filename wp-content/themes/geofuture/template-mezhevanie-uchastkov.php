<?php /* Template Name: Межевание участков */ ?>

<?php get_header(); ?>

<div class="modal mfp-hide" id="callback-mejevanie">
    <span class="modal-close">Закрыть</span>
    <?= do_shortcode( '[contact-form-7 id="255"]' ); ?>
</div>

<section class="block intro intro-survey">
    <div class="container">
        <h1 class="intro-title"><?= get_field( 'page-title' ); ?></h1>
        <h2 class="intro-subtitle"><?= get_field( 'page-subtitle' ); ?></h2>
        <h3 class="intro-topography-title intro-survey-title"><?= get_field( 'survey-title' ); ?></h3>
        <p class="intro-description-left">
            <?= get_field( 'survey-description-left' ); ?>
        </p>
        <div class="intro-callback-form">
            <?= do_shortcode( '[contact-form-7 id="105"]' ); ?>
        </div>
    </div>
</section>

<?php $s_advantages = get_field( 'survey-advantages' ); ?>
<?php if ( count( $s_advantages ) > 0 ) : ?>
<section class="block advantages survey-advantages">
    <div class="container">

        <?php foreach( $s_advantages as $s_advantage ) : ?>
        <div class="advantage-single">
            <article class="situation-single">
                <strong><?= $s_advantage['title']; ?></strong>
                <p><?= $s_advantage['text']; ?></p>
            </article>
        </div>
        <?php endforeach; ?>

    </div>
</section>
<?php endif; ?>

<?php require_once( 'include-blocks/advantages.php' ); ?>


<section class="block intro-testimonials service-testimonials" id="testimonials">
    <div class="container container-small">
        <h3 class="block-title">Начните сотрудничество с командой <br>профессионалов уже сегодня <strong>Прочитайте отзывы о работе с нами</strong></h3>
        <?php
        $indexPageID = get_id_by_slug( 'index' );
        $testimonials = get_field( 'page-reviews', $indexPageID ); ?>

        <?php if ( count( $testimonials ) > 0 ) : ?>
        <div class="testimonials-slider">
            <?php // filter testimonials by type
            $testimonials = array_filter( $testimonials, function($item) {
                if ( $item['type'] == 'Межевание' ) return $item;
            } ); ?>
            <?php $testimonials = array_chunk( $testimonials, 2 ); ?>

            <?php foreach ( $testimonials as $slide ) : ?>
            <div class="intro-testimonials-slide">

                <?php foreach ( $slide as $testimonial ) : ?>
                <article class="testimonial-single">
                    <p class="testimonial-single-info">
                        <?php if ( $testimonial['city'] ) : ?>
                        <span class="testimonial-single-city"><?= $testimonial['city']; ?></span>
                        <?php endif; ?>
                        <?= $testimonial['author']; ?>
                    </p>
                    <div class="testimonial-single-body">
                        <img src="<?= $testimonial['photo']; ?>" class="testimonial-single-author">
                        <p class="testimonial-single-text"><?= $testimonial['text']; ?></p>
                    </div>
                </article>
                <?php endforeach; ?>

            </div>
            <?php endforeach; ?>

        </div>
        <?php endif; ?>

    </div>
</section>
<section class="block pattern-block survey-calc">
    <div class="container">
        <h3 class="block-title">РАССЧИТАТЬ СТОИМОСТЬ МЕЖЕВАНИЯ <br>ЗЕМЕЛЬНОГО УЧАСТКА ПРЯМО СЕЙЧАС</h3>
    </div>
    <div class="survey-calc-container">
        <div class="container">
            <div class="survey-calc-info">
                <strong class="survey-calc-info-title">СПЕЦИАЛЬНОЕ ПРЕДЛОЖЕНИЕ <span>ДО 01.07.2016</span></strong>
                <p class="survey-calc-info-text">
                    Профессиональный вынос <br>
                    границ в натуру в подарок <br>
                    после межевания
                </p>
                <small>За сегодня мы рассчитали 43 участка</small>
            </div>
            <div class="survey-calc-form">
                <?= do_shortcode( '[contact-form-7 id="119"]' ); ?>
            </div>
        </div>
    </div>
</section>

<?php $cl_advantages = get_field( 'clients-advantages' ); ?>
<?php if ( count( $cl_advantages ) > 0 ) : ?>
<section class="block clients-advantages">
    <div class="container">
        <h3 class="block-title">Что получают заказчики, заказывая <br>у нас услугу межевания</h3>
    </div>
    <?php
        $cl_top = array_slice( $cl_advantages, 0, 3 );
        $cl_bottom = array_slice( $cl_advantages, 2, 3 );
    ?>
    <div class="clients-advantages-list">

        <?php if ( count ($cl_top ) > 0 ) : ?>
        <div class="clients-advantages-top">
            <div class="container">

                <?php foreach ( $cl_top as $cl_advantage ) : ?>
                <article class="clients-advantage-single">
                    <p><?= $cl_advantage['text']; ?></p>
                </article>
                <?php endforeach; ?>

            </div>
        </div>
        <?php endif; ?>

        <?php if ( count( $cl_bottom ) > 0 ) : ?>
        <div class="clients-advantages-bottom">
            <div class="container">

                <?php foreach ( $cl_bottom as $cl_advantage ) : ?>
                <article class="clients-advantage-single">
                    <p><?= $cl_advantage['text']; ?></p>
                </article>
                <?php endforeach; ?>

            </div>
        </div>
        <?php endif; ?>

    </div>
</section>
<?php endif; ?>

<?php $s_types = get_field( 'survey-types' ); ?>
<?php if ( count( $s_types ) > 0 ) : ?>
<section class="block survey-types">
    <div class="container">
        <h3 class="block-title">В каких случаях делается межевание <br>земельных участков</h3>
        <?php $s_types = array_chunk( $s_types, 4 ); ?>
        <div class="survey-types-slider">

            <?php foreach ( $s_types as $s_slide ) : ?>
            <div class="survey-types-slide">

                <?php foreach ( $s_slide as $s_type ) : ?>
                <div class="survey-single">
                    <div class="survey-single-info">
                        <h3 class="survey-single-title"><?= $s_type['title']; ?></h3>
                        <strong class="survey-single-price">
                            <?= $s_type['price']; ?>

                            <?php if ( $s_type['old-price'] ) : ?>
                                <span class="old"><?= $s_type['old-price']; ?></span>
                            <?php endif; ?>

                        </strong>
                    </div>
                    <article class="survey-single-text">
                        <p><?= $s_type['text']; ?></p>
                    </article>
                    <a href="#callback-mejevanie" class="button survey-single-button popup">Заказать</a>
                </div>
                <?php endforeach; ?>

            </div>
            <?php endforeach; ?>

        </div>
    </div>
</section>
<?php endif; ?>

<?php require_once( 'include-blocks/director-cadastral.php' ); ?>

<?php get_footer(); ?>
