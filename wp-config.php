<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'geofuture');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '*&Xek3DE`SDM84a VY=![h}?*B*C7<BHM4n_`.?Za_S+s*7~2g!~B+B>m|}p06E*');
define('SECURE_AUTH_KEY',  'vzd7V y(IDpjTEO2u8pn*:uCmXGSrN>0s MJAXXP11fI//xm3f)}]<wI&&#$t^x0');
define('LOGGED_IN_KEY',    ')&D./<YDXJ;`5D%Kx(_-f{FJ;U(M8[eJ{^U_LYx+CXMb[<84WQQaGYDh_Y $9b9)');
define('NONCE_KEY',        '~RYr&})YiS&)?2?#n?Gl^H( FiBa>Zx<uZ`rms>=CIj[U,Abg|NW4WlW={@D14S8');
define('AUTH_SALT',        'tQEy#|6 T,@8Y`M:/KC&m+_WwN``3}Da#+rx78mk:K#U`&6hac^vLJ/^L<#t_4YX');
define('SECURE_AUTH_SALT', 'mp/m[9BI:(v(c`hc%w?7d<sLw_oT+>8lA(Dzs)T6dX!^-(q|O& o2jBY(?D>yyr/');
define('LOGGED_IN_SALT',   'JI#e*6H$X8dq=@Ks2*6`ZpbTw`BaRELjxijimidC(|w~G@){t3`!RF%wMXNyAlG,');
define('NONCE_SALT',       'I<HUk&}#xs+eDpY|ohny_mFNE<pe?hNpDh<otP3TG-+xUj6WX(NjW(=W@)R^U.[%');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'gf_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

define('WP_HOME', 'http://geofuture.wp');
define('WP_SITEURL', 'http://geofuture.wp');
define('WPCF7_AUTOP', false);

define('WP_MEMORY_LIMIT', '2048M');

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
